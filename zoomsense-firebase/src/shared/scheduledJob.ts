import { ZoomSenseFirebaseRoot } from "../types/zoomSenseFirebaseRoot";
import { NestedValueOf } from "./utils";

/**
 * Typesafe function to schedule the setting of a value at a Firebase path at some point in the future.
 * @param db The Firebase database object, i.e. an instance of ZoomSenseClientFirebaseDB or ZoomSenseServerFirebaseDB.
 * @param timestamp The time the value should be set, as a unix-style timestamp, i.e. milliseconds since the epoch.
 * @param meetingId The meetingId associated with this scheduled update.  This value is only used for cancelling jobs
 * associated with a meeting (e.g. if the meeting advances to a new section), and does not otherwise have to match a
 * real meeting in the system.  If you wish to schedule an update that is independent of any specific meeting, pass in
 * something meaningful like "global" as the meetingId.
 * @param path The path string specifying where in the Firebase RTDB the value should be set.
 * @param value The value to set at the given path.
 * @returns A promise which resolves to the "job id" of the change, which can be used to subsequently cancel the
 * scheduled change (assuming it has not yet been applied).
 */
export async function createScheduledFirebaseSet<P extends string, Base = ZoomSenseFirebaseRoot>(
  db: any,
  timestamp: number,
  meetingId: string,
  path: P,
  value: NestedValueOf<Base, P>
  ): Promise<string> {
  const jobRef = await db.ref(`zoomSenseSchedule/meeting/${meetingId}/job`).push({
    timestamp,
    meetingId,
    path,
    value
  });
  return jobRef.key!;
}

/**
 * Cancel a previously-scheduled setting of a value in the Firebase RTDB (created by calling `scheduleFirebaseSet`).  If
 * the scheduled change has already been executed, this call will do nothing.
 * @param db The Firebase database object.
 * @param meetingId The meetingId previously used to schedule the change.
 * @param jobId (optional) If supplied, only the scheduled change with the matching jobId is cancelled; otherwise, all
 * pending scheduled changes associated with the given meetingId are cancelled.
 */
export async function cancelScheduledFirebaseSet(
  db: any,
  meetingId: string,
  jobId?: string
) {
  if (jobId) {
    await db.ref(`zoomSenseSchedule/meeting/${meetingId}/job/${jobId}`).remove();
  } else {
    await db.ref(`zoomSenseSchedule/meeting/${meetingId}`).remove();
  }
}