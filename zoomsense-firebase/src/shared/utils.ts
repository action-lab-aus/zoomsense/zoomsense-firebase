/**
 * This file contains some useful utility types used throughout the codebase
 */

/**
 * In some cases, it is useful to specify that we want the child of some Object, where that
 * object only has "dynamic" keys. i.e. [key: string]
 */
export type ChildOfDynamicStringKey<T> = T extends { [key: string]: string }
  ? string extends keyof T
    ? T[string]
    : void
  : number | string extends keyof T
  ? T[keyof T]
  : void;

/**
 * If Key is a key of T, return T[Key], else void (handles the special case where
 * Javascript implicitly converts numbers to strings, see first paragraph of:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_Accessors#property_names)
 */
export type TypeOfStringOrNumberField<T, Key> = Key extends keyof T
  ? Required<T>[Key]
  : Key extends `${number}`
  ? T extends { [key: number]: unknown }
    ? Required<T>[number]
    : void
  : void;

/**
 * Drills down into a type, treating Key as a slash separated list of keys
 * (it also strips any leading slashes). If the given path is invalid, resolves
 * to void.
 *
 * Example: NextedValueOf<{ a: { b: 'c' } }, '/a/b'> resolves to 'c'
 *
 * Note: Trailing slashes will add an extra '' key, which will probably be invalid:
 * NextedValueOf<{ a: { b: 'c' } }, '/a/b/'> resolves to void
 */
export type NestedValueOf<T, Key extends string> = T extends object
  ? Key extends `/${infer SubKeys}`
    ? NestedValueOf<T, SubKeys>
    : Key extends `${infer KeyOfT}/${infer SubKeys}`
    ? NestedValueOf<TypeOfStringOrNumberField<T, KeyOfT>, SubKeys>
    : TypeOfStringOrNumberField<T, Key>
  : void;
