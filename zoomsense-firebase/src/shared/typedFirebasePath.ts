import { NestedValueOf } from "./utils";
import { ZoomSenseFirebaseRoot } from "../types/zoomSenseFirebaseRoot";

export type TypedFirebasePath<T, P extends string> = P & { _firebaseType: T };

export function buildFirebasePath<P extends string, Base = ZoomSenseFirebaseRoot>(
  path: P
): TypedFirebasePath<NestedValueOf<Base, P>, P> {
  return path as TypedFirebasePath<NestedValueOf<Base, P>, P>;
}

export function appendFirebasePath<T, P extends string, S extends string>(
  path: TypedFirebasePath<T, P>,
  suffix: S
): TypedFirebasePath<NestedValueOf<T, S>, `${P}/${S}`> {
  return `${path}/${suffix}` as TypedFirebasePath<NestedValueOf<T, S>, `${P}/${S}`>;
}
