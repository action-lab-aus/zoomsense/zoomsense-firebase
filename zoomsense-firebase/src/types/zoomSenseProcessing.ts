export interface ZoomSenseProcessingRecordingJob {
  created: string;
  fileBucket: string;
  filePath: string;
}

export interface ZoomSenseProcessing {
  recordings: {
    [guid: string]: ZoomSenseProcessingRecordingJob;
  };
}
