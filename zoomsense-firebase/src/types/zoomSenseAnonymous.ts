export interface ZoomSenseAnonymousToken {
  token: string;
}

export interface ZoomSenseAnonymous {
  /** RTDB path anonymous/keys doesn't appear to be referenced in any code? */
  keys: {
    [unknownHexString: string]: {
      token: string;
    };
  };
  tokens: {
    [hostUserId: string]: {
      [meetingId: string]: ZoomSenseAnonymousToken;
    };
  };
  users: {
    [userId: string]: {
      hosts: {
        [hostUserId: string]: {
          createdAt: number;
        };
      };
      meetings: {
        [meetingId: string]: {
          createdAt: number;
        };
      };
    };
  };
}
