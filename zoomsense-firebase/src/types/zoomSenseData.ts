export interface ZoomSenseActiveSpeakers {
  isInBO?: boolean;
  kickOutTimestamp?: number;
  isRecording?: boolean;
  hasRecording?: boolean;
  recordingStart?: number;
  activeHistory?: {
    [historyId: string]: {
      zoomid: number[];
      timestamp: number;
    };
  };
  userList?: {
    [zoomId: number]: {
      persistentUid: string;
      username: string;
      userRole: string;
      joinedAt: number;
    };
  };
}

export interface ZoomSenseChat {
  isInBO?: boolean;
  msg: string;
  msgSender: number;
  msgSenderName: string;
  msgReceiver: number;
  msgReceiverName: string;
  timestamp: number;
}

// Each time a user enters or exits a zoom room, the zoomsensor logs
// it
export type EntryExitEvent = {
  // Whether the user is entering/exiting a breakout room
  isInBO: boolean;
  // Either "enter" or "leave", indicating whether this event represents
  // a user joining or leaving the room
  status: string;
  timestamp: number;
  // User UID
  uid: string;
  persistentUid?: string; // This is populated on "enter" events, but not "leave" events :(
  userRole: string;
  username: string;
};

export type ZoomSenseMeetingChatNode = {
  isInBO?: boolean;
  leave?: {
    leave: boolean | string;
  };
  message?: {
    [key: string]: {
      receiver: number;
      msg: string;
    };
  };
} & {
  [chatId: string]: ZoomSenseChat;
};

export interface ZoomSenseDataMeetingMeta {
  /** UTC time code in ISO 8601 format. */
  actualEndTime?: string;
  /** Array of the names of the sections in this meeting */
  sections?: string[];
}

/**
 * Individual plugins can use declaration merging to add their own types to this interface in their codebase. For
 * example:
 *
 * ```
 * declare module "@zoomsense/zoomsense-firebase" {
 *   export interface ZoomSenseDataPlugins {
 *     gdocs: GDocsPluginRootType;
 *   }
 * }
 * ```
 */
export interface ZoomSenseDataPlugins {}

export interface ZoomSenseDataLogsBOMetaMeetingSensor {
  current: { isInBO: boolean };
  history: {
    [timestamp: number]: {
      isInBO: boolean;
    };
  };
}

export interface ZoomSenseDataLogsBOMetaMeeting {
  [sensorId: string]: ZoomSenseDataLogsBOMetaMeetingSensor;
}

export interface ZoomSenseDataNameChange {
  [sensorId: string]: {
    [eventId: string]: {
      isInBO: boolean;
      timestamp: number;
      userId: string;
      persistentUid: string;
      userName: string;
      userRole: string;
    };
  };
}

export interface ZoomSenseCurrentUserInMeeting {
  userId: string;
  username: string;
  userRole: string;
}

export interface ZoomSenseCurrentUsersForMeeting {
  [sensorId: string]: {
    [userId: string]: ZoomSenseCurrentUserInMeeting;
  };
}

export interface ZoomSenseData {
  activeSpeakers?: {
    [meetingId: string]: {
      [sensorId: string]: {
        current: ZoomSenseActiveSpeakers;
        history?: {
          [historyTimestamp: number]: ZoomSenseActiveSpeakers;
        };
      };
    };
  };
  chats?: {
    [meetingId: string]: {
      [displayName: string]: ZoomSenseMeetingChatNode;
    };
  };
  plugins?: ZoomSenseDataPlugins;
  logs?: {
    // Copy of active speaker history
    boMeta?: {
      [meetingId: string]: ZoomSenseDataLogsBOMetaMeeting;
    };
    // Events for each time a user enters or exits a room
    entryExit?: {
      [meetingId: string]: {
        [sensorId: string]: {
          [entryExitId: string]: EntryExitEvent;
        };
      };
    };
  };
  currentUsers?: {
    [meetingId: string]: ZoomSenseCurrentUsersForMeeting;
  };
  nameChange?: {
    [meetingId: string]: ZoomSenseDataNameChange;
  };
  meetingMeta?: {
    [meetingId: string]: ZoomSenseDataMeetingMeta;
  };
  sectionChange?: {
    [meetingId: string]: {
      _history: {
        [timestamp: number]: {
          currentSection: number;
        };
      };
    };
  };
}
