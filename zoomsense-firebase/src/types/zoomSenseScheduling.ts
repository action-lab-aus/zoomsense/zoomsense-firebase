
export interface ZoomSenseSchedulingSensor {
  startTime: string;
  /** Zoom meeting ID */
  uid: string;
  /** Zoom meeting passcode */
  password: string;
  webinarToken?: string;
  lastSeen?: number;
}

export interface ZoomSenseScheduling {
  /** key is the IP of the VM, with dots replaced with underscores */
  [vmName: string]: {
    capacity: number;
    sensors?: {
      /** key is in the format (`MeetingId`)-(sensor index, starting from 1) */
      [sensorIndex: string]: ZoomSenseSchedulingSensor;
    };
    leaving?: {
      /** key is in the format (meeting id without start time)-ZoomSensor_(sensor index, starting from 1) */
      [sensor: string]: true;
    };
  };
}
