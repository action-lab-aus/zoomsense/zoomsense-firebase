export enum ZoomUserType {
  Basic = 1,
  Licensed = 2,
  OnPrem = 3,
  None = 99,
}

export enum ZoomUserLoginType {
  FacebookOAuth = 0,
  GoogleOAuth = 1,
  AppleOAuth = 24,
  MicrosoftOAuth = 27,
  MobileDevice = 97,
  RingCentralOAuth = 98,
  APIUser = 99,
  ZoomWorkEmail = 100,
  SingleSignOn = 101,
}

export enum ZoomUserPhoneNumberLabel {
  Mobile = "Mobile",
  Office = "Office",
  Home = "Home",
  Fax = "Fax",
}

export enum ZoomUserPronounOption {
  AskWhenJoinMeeting = 1,
  DisplayInMeetings = 2,
  DoNotDisplayInMeetings = 3,
}

export enum ZoomUserVerified {
  AccountNotVerified = 0,
  AccountVerified = 1,
}

export enum ZoomUserStatus {
  pending = "pending",
  active = "active",
  inactive = "inactive",
}

export type ZoomUser = {
  id: string;
  created_at: string; // date-time
  dept: string;
  email: string;
  first_name: string; // <= 64 characters
  last_name: string; // <= 64 characters
  last_client_version: string;
  last_login_time: string; // date-time
  pmi: number; // Personal Meeting ID
  role_name: string;
  timezone: string;
  type: ZoomUserType;
  use_pmi: boolean; // Use the personal meeting ID for instant meetings
  account_id: string;
  account_number: number;
  cms_user_id?: string; // CMS ID of user, only enabled for Kaltura integration.
  company: string;
  custom_attributes: { key: string; name: string; value: string }[];
  employee_unique_id?: string;
  group_ids: string[];
  host_key: string;
  im_group_ids: string[];
  jid: string;
  job_title: string;
  language: string;
  location: string;
  login_type: ZoomUserLoginType;
  manager: string;
  personal_meeting_url: string;
  phone_country?: string; // Deprecated
  phone_number?: string; // Deprecated
  phone_numbers: {
    code: string;
    country: string;
    label: ZoomUserPhoneNumberLabel;
    number: string;
    verified: boolean;
  }[];
  pic_url: string; // The URL for user's profile picture
  plan_united_type?: string; // United plan type. Only returned if user is enrolled in the Zoom United plan.
  pronouns: string;
  pronouns_option: ZoomUserPronounOption;
  role_id: string;
  status: ZoomUserStatus;
  vanity_url: string;
  verified: ZoomUserVerified;
  cluster: string;
};

export type ZoomOAuthToken = {
  access_token: string;
  token_type: string;
  expires_in: number;
  scope: string;
  refresh_token?: string;
};

export enum ZoomWebinarType {
  Webinar = 5,
  RecurringWebinarNoFixedTime = 6,
  RecurringWebinarFixedTime = 9,
}

export type ZoomWebinarSummary = {
  uuid: string;
  id: string;
  host_id: string;
  topic: string;
  type: ZoomWebinarType;
  duration: number; // in minutes
  timezone: string;
  created_at: string; // date-time
  join_url: string;
};

/**
 * Response from GET https://api.zoom.us/v2/users/{userId}/webinars
 */
export type ZoomWebinarsResponse = {
  next_page_token?: string;
  page_count: number;
  page_number: number;
  page_size: number;
  total_records: number;
  webinars: ZoomWebinarSummary[];
};

export type ZoomMeetingOccurrence = {
  duration: number;
  occurrence_id: string; // Unique Identifier that identifies an occurrence of a recurring webinar.
  start_time: string; // date-time
  status: string; // Occurrence status.
};

export enum ZoomMeetingRecurrenceType {
  Daily = 1,
  Weekly = 2,
  Monthly = 3,
}

export type ZoomMeetingRecurrence = {
  end_date_time: string; // date-time, the final date on which the meeting will recur before it is cancelled. (Cannot be used with "end_times".)
  end_times: number; // <= 365, how many times the meeting should recur before it is cancelled. (Cannot be used with "end_date_time".)
  monthly_day: number; // Use this field only if you're scheduling a recurring meeting of type 3 to state which day in a month the meeting should recur. The value range is from 1 to 31.
  monthly_week: number; // Use this field only if you're scheduling a recurring meeting of type 3 to state the week of the month when the meeting should recur. Valid values are 1, 2, 3, 4 or -1 (last week of the month)
  monthly_week_day: number; // Use this field only if you're scheduling a recurring meeting of type 3 to state a specific day in a week (1 = Sunday to 7 = Saturday) when the monthly meeting should recur. To use this field, you must also use the monthly_week field.
  repeat_interval: number; // Define the interval at which the meeting should recur. For instance, if you would like to schedule a meeting that recurs every two months, you must set the value of this field as 2 and the value of the type parameter as 3.
  type: ZoomMeetingRecurrenceType;
  weekly_days: string; // This field is required if you're scheduling a recurring meeting of type 2 to state which day(s) of the week the meeting should repeat. The value for this field is comma-separated numbers between 1 to 7 in string format ("1" = Sunday, "7" = Saturday)
};

export enum ZoomMeetingStatus {
  waiting = "waiting",
  started = "started",
}

// See https://support.zoom.us/hc/en-us/articles/115000293426-Scheduling-Tracking-Fields
export type ZoomMeetingTrackingField = {
  field: string; // label of the tracking field
  value: string; // value for the field
  visible: boolean; // Indicates whether the tracking field is visible in the meeting scheduling options in the Zoom Web Portal or not.
};

export enum ZoomMeetingType {
  InstantMeeting = 1,
  ScheduledMeeting = 2,
  RecurringMeetingNoFixedTime = 3,
  PMIMeeting = 4,
  RecurringMeetingFixedTime = 8,
}

export type ZoomMeeting = {
  assistant_id: string; // The ID of the user who scheduled this meeting on behalf of the host.
  host_email: string; // Email address of the meeting host.
  host_id: string; // ID of the user who is set as host of meeting.
  id: number; // Unique identifier of the meeting in "long" format (represented as int64 data type in JSON), also known as the meeting number.
  uuid: string; // Unique meeting ID. Each meeting instance will generate its own Meeting UUID (i.e., after a meeting ends, a new UUID will be generated for the next instance of the meeting).
  agenda: string;
  created_at: string; // date-time
  duration: number;
  encrypted_password: string; // Encrypted passcode for third party endpoints (H323/SIP).
  h323_password: string; // H.323/SIP room system passcode.
  occurrences: ZoomMeetingOccurrence[];
  password?: string; // Meeting passcode
  pmi: string; // Personal Meeting ID, used for scheduled meetings and recurring meetings with no fixed time.
  pre_schedule: boolean; // Whether the prescheduled meeting was created via the GSuite app.
  recurrence: ZoomMeetingRecurrence[]; // for a meeting with type 8 i.e., a recurring meeting with fixed time.
  settings: unknown[]; // Settings is a huge, nested object. I'm not going to bother defining its type unless we need it.
  start_time?: string; // date-time. will not be returned if the meeting is an instant meeting.
  start_url: string; // a URL which a host or an alternative host can use to start the Meeting.
  status: ZoomMeetingStatus;
  timezone: string;
  topic?: string;
  tracking_fields: ZoomMeetingTrackingField[];
  type: ZoomMeetingType;
};

/**
 * Response from GET https://api.zoom.us/v2/webinars/{webinarId}
 */
export interface ZoomWebinar {
  uuid: string;
  id: string;
  host_id: string;
  topic: string;
  type: ZoomWebinarType;
  start_time: string; // date-time
  duration: number; // in minutes
  timezone: string;
  agenda: string;
  created_at: string; // date-time
  start_url: string;
  join_url: string;
  occurrences: ZoomMeetingOccurrence[];
  settings: {
    host_video: boolean;
    panelists_video: boolean;
    practice_session: boolean;
    hd_video: boolean;
    approval_type: number;
    registration_type: number;
    audio: string;
    auto_recording: string;
    enforce_login: boolean;
    enforce_login_domains: string;
    alternative_hosts: string;
    close_registration: boolean;
    show_share_button: boolean;
    allow_multiple_devices: boolean;
  };
}