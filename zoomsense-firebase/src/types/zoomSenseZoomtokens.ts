export interface ZoomSenseZoomToken {
  /** The user's authorization code returned during their oAuth login. */
  auth: string;
  /** Current access token for making requests on behalf of the given user to Zoom's APIs */
  access: string;
  /** Timestamp when the access token will expire. */
  access_expires: number;
  /** Refresh token used to retrieve a fresh access token after the current one has expired. */
  refresh: string;
}

export interface ZoomSenseZoomtokens {
  [userId: string]: ZoomSenseZoomToken;
}
