import { ZoomMeetingType } from "./zoomTypes";

export enum ZoomSenseModule {
  prompts = "prompts",
}

export interface ZoomSenseSensorMeetingRecord {
  version: string;
}

export type ZoomSenseMeeting = { [zoomSensorName: `ZoomSensor_${number}`]: ZoomSenseSensorMeetingRecord } & {
  /** Zoom meeting id */
  id: number;
  isWebinar: boolean;
  webinarToken?: string;
  noOfBreakoutRooms: number;
  /** Zoom meeting passcode */
  password: string;
  requested: boolean;
  scheduled?: boolean;
  scheduledManually?: boolean;
  /** UTC time code in ISO 8601 format. */
  deleted?: string;
  shareLink: string;
  template: string | null;
  topic: string;
  type: ZoomMeetingType | "webinar" | "Vonage";
  /** UTC time code in ISO 8601 format. */
  startTime: string;
  /** UTC time code in ISO 8601 format. */
  endTime: string;
  /** UTC time code in ISO 8601 format. */
  actualStartTime?: string;
  /** UTC time code in ISO 8601 format. */
  actualEndTime?: string;
  modules: { [key in ZoomSenseModule]: boolean };
  error?: string;
  unallocated?: number;
  vmAllocation?: {
    /** The IPs of the VMs (with dots replaced with underscores) each zoom sensor in the meeting has been allocated on */
    [zoomSensorName: string]: string;
  };
};

/** Meeting IDs are formatted: (numeric Zoom meeting ID)_(start time in "{YYYYMMDD}T{HHmmss}" format) */
export type MeetingId = string;

export interface ZoomSenseMeetings {
  [userId: string]: {
    [meetingId: MeetingId]: ZoomSenseMeeting;
  };
}
