export interface ZoomSenseReallocation {
  /** key is in the format (meeting id without start time)-ZoomSensor_(sensor index, starting from 1) */
  [sensor: string]: {
    startTime: string;
    password: string;
    uid: string;
    meetingNo: string;
    sensorIdx: number;
    webinarToken?: string;
  };
}