export interface ZoomSenseScheduledJob {
  timestamp: number;
  meetingId: string;
  path: string;
  value: any;
}

export interface ZoomSenseScheduledJobMeeting {
  job?: {
    [jobId: string]: ZoomSenseScheduledJob;
  };
}

export interface ZoomSenseSchedule {
  nextCheckTimestamp: number;
  meeting?: {
    [meetingId: string]: ZoomSenseScheduledJobMeeting;
  };
}
