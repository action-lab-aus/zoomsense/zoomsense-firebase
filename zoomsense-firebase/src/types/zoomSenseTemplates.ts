import { ZoomSenseConfigMeetingScheduleSchedule } from "./zoomSenseConfig";

export interface ZoomSenseTemplate {
  name: string;
  raw: string;
  schedule: ZoomSenseConfigMeetingScheduleSchedule;
}

export interface ZoomSenseTemplates {
  [templateName: string]: ZoomSenseTemplate;
}