import { ZoomSenseAnonymous } from "./zoomSenseAnonymous";
import { ZoomSenseArchiveRef } from "./zoomSenseArchiveRef";
import { ZoomSenseConfig } from "./zoomSenseConfig";
import { ZoomSenseData } from "./zoomSenseData";
import { ZoomSenseMeetings } from "./zoomSenseMeetings";
import { ZoomSenseProcessing } from "./zoomSenseProcessing";
import { ZoomSenseReallocation } from "./zoomSenseReallocation";
import { ZoomSenseScheduling } from "./zoomSenseScheduling";
import { ZoomSenseWhitelist } from "./zoomSenseWhitelist";
import { ZoomSenseZoomtokens } from "./zoomSenseZoomtokens";
import { ZoomSenseSchedule } from "./zoomSenseSchedule";
import { ZoomSenseTemplates } from "./zoomSenseTemplates";

export interface ZoomSenseFirebaseRoot {
  anonymous?: ZoomSenseAnonymous;
  archiveRef?: ZoomSenseArchiveRef;
  config?: ZoomSenseConfig;
  data?: ZoomSenseData;
  meetings?: ZoomSenseMeetings;
  processing?: ZoomSenseProcessing;
  reallocation?: ZoomSenseReallocation;
  scheduling?: ZoomSenseScheduling;
  templates?: ZoomSenseTemplates;
  whitelist?: ZoomSenseWhitelist;
  zoomtokens?: ZoomSenseZoomtokens;
  zoomSenseSchedule?: ZoomSenseSchedule;
}
