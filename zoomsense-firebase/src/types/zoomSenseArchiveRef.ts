/** RTDB path archiveRef doesn't appear to be referenced in any code? */
export interface ZoomSenseArchiveRef {
  [unknownGuid: string]: {
    coachUid: string;
    meetingId: string;
  };
}
