/** Array of email addresses of users allowed to schedule ZoomSense meetings. */
export type ZoomSenseWhitelist = string[];
