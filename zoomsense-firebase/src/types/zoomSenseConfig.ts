import { MeetingId } from "./zoomSenseMeetings";

/** Plugins can store arbitrary data in their node, but also have these two fields. */
export interface ZoomSensePlugin {
  enabled: boolean;
  global?: true;
}

export interface ZoomSenseConfigCurrentStatePlugins {
  [pluginName: string]: ZoomSensePlugin;
}

export interface ZoomSenseActivePluginConfig {
  /** The plugin's name */
  plugin: string;
  /** Arbitrary settings for the plugin, which will be merged with the `ZoomSensePlugin` type */
  settings: object;
}

export enum ZoomSenseScheduleAdvanceType {
  manual = "manual",
  timed = "timed",
  auto = "auto",
}

export type ZoomSenseScheduleAdvance =
  {
    type: ZoomSenseScheduleAdvanceType.manual | ZoomSenseScheduleAdvanceType.auto;
  } | {
    type: ZoomSenseScheduleAdvanceType.timed;
    /** A "human interval" string describing the duration of the section (e.g. "5 minutes", "90 seconds" and similar) */
    when: string;
    /** A flag that indicates if the timer should be automatically paused at the start of the section. */
    initiallyPaused?: boolean;
  };

export interface ZoomSenseConfigMeetingCurrentCurrentState {
  /** Timestamp that the current section started (i.e. finished loading). */
  startedAt: number;
  /** Timestamp when the current section will end (only set in timed rounds when not paused) */
  endsAt?: number;
  /** The 0-based index of the current section, set when the data is actually loaded. */
  sectionLoaded?: number;
  /** Used instead of sectionLoaded in the template editor. */
  demoSection?: number;
  plugins?: ZoomSenseConfigCurrentStatePlugins;
  /**
   * Used in timed rounds to indicate whether the timer is currently paused or not.  If set, the timer is paused,
   * and the value is the number of milliseconds remaining in the section at the time it was paused.
   */
  paused?: number;
  /**
   * The jobId of the scheduled job to advance to the next section (if any), so it can be cancelled when the timer
   * is paused.
   */
  advanceSectionJobId?: string;
}

export interface ZoomSenseConfigMeetingCurrent {
  /** The 0-based index of the current section. Changing it triggers the update of `currentState`. */
  currentSection: number;
  currentState?: ZoomSenseConfigMeetingCurrentCurrentState;
}

export interface ZoomSenseConfigMeetingScheduleScheduleScript {
  advance: ZoomSenseScheduleAdvance;
  name: string;
  plugins: ZoomSenseActivePluginConfig[];
}

export interface ZoomSenseConfigMeetingScheduleSchedule {
  /** The global plugins will be active in every section. */
  global: ZoomSenseActivePluginConfig[];
  /** `currentSection` is used as an index into `script` to determine which other plugins are active in that section. */
  script: ZoomSenseConfigMeetingScheduleScheduleScript[];
}

export interface ZoomSenseConfigMeetingSchedule {
  name?: string;
  tags?: string[];
  /** YAML representation of the schedule data (except for `raw` itself, obviously.) */
  raw?: string;
  schedule: ZoomSenseConfigMeetingScheduleSchedule;
}

export interface ZoomSenseConfigMeeting {
  current?: ZoomSenseConfigMeetingCurrent;
  schedule?: ZoomSenseConfigMeetingSchedule;
  /**
   * This user ID is set on demo meetings (when editing a template) for RTDB permission reasons, because there is no
   * corresponding real meeting node under /meetings/${uid}/${meetingId}.
   */
  uid?: string;
}

export interface ZoomSenseConfig {
  [meetingId: MeetingId]: ZoomSenseConfigMeeting;
}
