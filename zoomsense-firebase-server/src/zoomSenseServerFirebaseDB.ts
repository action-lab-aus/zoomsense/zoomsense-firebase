import { database } from "firebase-admin";
import { FirebaseApp } from "@firebase/app-types";
import {
  ChildOfDynamicStringKey,
  NestedValueOf,
  TypedFirebasePath,
  ZoomSenseFirebaseRoot
} from "@zoomsense/zoomsense-firebase";

export interface TypedDataSnapshot<T, Base = ZoomSenseFirebaseRoot, Exists extends boolean = boolean> extends database.DataSnapshot {
  child<P extends string>(path: P): TypedDataSnapshot<NestedValueOf<T, P>, Base>;
  forEach(action: (a: TypedDataSnapshot<T[keyof T], Base, true>) => boolean | void): boolean;
  ref: TypedReference<T, Base>;
  exists(this: TypedDataSnapshot<T,  Base, Exists>): this is TypedDataSnapshot<T,  Base, true>;
  val(): Exists extends true ? T : Exists extends false ? null : T | null;
}

export type TypedThenableReference<T, Base> = TypedReference<T, Base> &
  Pick<Promise<TypedReference<T, Base>>, "then" | "catch">;

export type TypedQuery<T, Base = ZoomSenseFirebaseRoot> = Pick<TypedReference<T, Base>, keyof database.Query>;

export interface TypedReference<T, Base = ZoomSenseFirebaseRoot> extends database.Reference {
  ref: TypedReference<T, Base>;
  child<K extends string>(path: K): TypedReference<NestedValueOf<T, K>, Base>;
  root: TypedReference<Base, Base>;
  push(
    value?: ChildOfDynamicStringKey<T>,
    onComplete?: (a: Error | null) => any
  ): TypedThenableReference<ChildOfDynamicStringKey<T>, Base>;
  set(value: T, onComplete?: (a: Error | null) => any): Promise<unknown>;
  get(): Promise<TypedDataSnapshot<T, Base>>;
  update(values: T extends object ? Partial<T> : never, onComplete?: (a: Error | null) => any): Promise<unknown>;
  on(
    eventType: string,
    callback: (a: TypedDataSnapshot<T, Base>, b?: string | null) => unknown,
    cancelCallbackOrContext?: ((a: Error) => any) | Object | null,
    context?: Object | null
  ): (a: database.DataSnapshot | null, b?: string | null) => unknown;
  off(
    eventType?: string,
    callback?: (a: TypedDataSnapshot<T, Base>, b?: string | null) => any,
    context?: Object | null
  ): void;
  once(
    eventType: string,
    successCallback?: (a: TypedDataSnapshot<T, Base>, b?: string | null) => any,
    failureCallbackOrContext?: ((a: Error) => void) | Object | null,
    context?: Object | null
  ): Promise<TypedDataSnapshot<T, Base>>;
  endBefore(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  endAt(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  equalTo(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  isEqual(other: TypedQuery<T, Base> | null): boolean;
  limitToFirst(limit: number): TypedQuery<T, Base>;
  limitToLast(limit: number): TypedQuery<T, Base>;
  orderByChild(path: string): TypedQuery<T, Base>;
  orderByKey(): TypedQuery<T, Base>;
  orderByPriority(): TypedQuery<T, Base>;
  orderByValue(): TypedQuery<T, Base>;
  startAt(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  startAfter(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
}

export class ZoomSenseServerFirebaseDB<Base = ZoomSenseFirebaseRoot> implements database.Database {
  private readonly db: database.Database;
  app: FirebaseApp;

  constructor(db: database.Database) {
    this.db = db;
    this.app = db.app;
  }

  ref<T, P extends string>(path: TypedFirebasePath<T, P>): TypedReference<T, Base>;
  ref<P extends string>(path: P): TypedReference<NestedValueOf<Base, P>, Base>;
  ref(path: string) {
    return this.db.ref(path);
  }

  goOffline(): void {
    this.db.goOffline();
  }

  goOnline(): void {
    this.db.goOnline();
  }

  refFromURL(url: string): database.Reference {
    return this.db.refFromURL(url);
  }

  useEmulator(host: string, port: number): void {
    this.db.useEmulator(host, port);
  }

  getRules(): Promise<string> {
    return this.db.getRules();
  }

  getRulesJSON(): Promise<object> {
    return this.db.getRulesJSON();
  }

  setRules(source: string | Buffer | object): Promise<void> {
    return this.db.setRules(source);
  }
}
