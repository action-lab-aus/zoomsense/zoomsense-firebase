import { RefBuilder } from "firebase-functions/lib/providers/database";
import { Change } from "firebase-functions";
import { CloudFunction, EventContext } from "firebase-functions/lib/cloud-functions";
import { DataSnapshot } from "firebase-functions/lib/common/providers/database";
import { FeaturesList } from "firebase-functions-test/lib/features";
import { NestedValueOf, TypeOfStringOrNumberField, ZoomSenseFirebaseRoot } from "@zoomsense/zoomsense-firebase";

import { TypedReference } from "./zoomSenseServerFirebaseDB";

type TypeOfContextVariable<T, Key extends string, ContextParams> = Key extends `{${infer CapturingVariable}}`
  ? keyof T extends number
    ? {
        [Key in keyof ContextParams | CapturingVariable]: Key extends keyof ContextParams ? ContextParams[Key] : number;
      }
    : T extends { [key: string]: unknown }
    ? {
        [Key in keyof ContextParams | CapturingVariable]: Key extends keyof ContextParams ? ContextParams[Key] : string;
      }
    : T extends { [key: number]: unknown }
    ? {
        [Key in keyof ContextParams | CapturingVariable]: Key extends keyof ContextParams ? ContextParams[Key] : number;
      }
    : void
  : ContextParams;

type ContextParamsType<T, Key extends string, Context = {}> = T extends object
  ? Key extends `/${infer SubKeys}`
    ? ContextParamsType<T, SubKeys, Context>
    : Key extends `${infer KeyOfT}/${infer SubKeys}`
    ? ContextParamsType<TypeOfCapturingVariableOrField<T, KeyOfT>, SubKeys, TypeOfContextVariable<T, KeyOfT, Context>>
    : TypeOfContextVariable<T, Key, Context>
  : void;

type TypeOfCapturingVariableOrField<T, Key> = Key extends `{${infer CapturingVariable}}`
  ? T extends { [key: number]: unknown }
    ? T[number]
    : T extends { [key: string]: unknown }
    ? T[string]
    : void
  : TypeOfStringOrNumberField<T, Key>;

// The strings used by function ref builders have a different syntax to that of the database ref strings - they can
// contain capturing variable names enclosed in curly brackets.
type NestedBuilderValueOf<T, Key extends string> = T extends object
  ? Key extends `/${infer SubKeys}`
    ? NestedBuilderValueOf<T, SubKeys>
    : Key extends `${infer KeyOfT}/${infer SubKeys}`
    ? NestedBuilderValueOf<TypeOfCapturingVariableOrField<T, KeyOfT>, SubKeys>
    : TypeOfCapturingVariableOrField<T, Key>
  : void;

export interface TypedRefBuilderDataSnapshot<T, Base = ZoomSenseFirebaseRoot, Exists extends boolean = boolean> extends DataSnapshot {
  get ref(): TypedReference<T, Base>;
  child<P extends string>(path: P): TypedRefBuilderDataSnapshot<NestedValueOf<T, P>, Base>;
  forEach(action: (a: TypedRefBuilderDataSnapshot<T[keyof T], Base, true>) => boolean | void): boolean;
  exists(this: TypedRefBuilderDataSnapshot<T,  Base, Exists>): this is TypedRefBuilderDataSnapshot<T,  Base, true>;
  val(): Exists extends true ? T : Exists extends false ? null : T | null;
}

export interface TypedEventContext<ContextParams> extends EventContext {
  params: ContextParams;
}

export interface TypedRefBuilder<T, ContextParams, Base> extends RefBuilder {
  onWrite(
    handler: (
      change: Change<TypedRefBuilderDataSnapshot<T, Base>>,
      context: TypedEventContext<ContextParams>
    ) => PromiseLike<any> | any
  ): CloudFunction<Change<TypedRefBuilderDataSnapshot<T>>>;
  onUpdate(
    handler: (
      change: Change<TypedRefBuilderDataSnapshot<T, Base, true>>,
      context: TypedEventContext<ContextParams>
    ) => PromiseLike<any> | any
  ): CloudFunction<Change<TypedRefBuilderDataSnapshot<T>>>;
  onCreate(
    handler: (
      snapshot: TypedRefBuilderDataSnapshot<T, Base, true>,
      context: TypedEventContext<ContextParams>
    ) => PromiseLike<any> | any
  ): CloudFunction<TypedRefBuilderDataSnapshot<T>>;
  onDelete(
    handler: (
      snapshot: TypedRefBuilderDataSnapshot<T, Base, true>,
      context: TypedEventContext<ContextParams>
    ) => PromiseLike<any> | any
  ): CloudFunction<TypedRefBuilderDataSnapshot<T>>;
}

export function typedFirebaseFunctionRef<T = ZoomSenseFirebaseRoot>(
  ref: (path: string) => RefBuilder
): <P extends string>(path: P) => TypedRefBuilder<NestedBuilderValueOf<T, P>, ContextParamsType<T, P>, T> {
  return (path) => ref(path) as any;
}

export function makeTypedTestDataSnapshot<P extends string, Base = ZoomSenseFirebaseRoot, T = NestedValueOf<Base, P>>(
  testRunner: FeaturesList,
  path: P,
  data: T | null
): TypedRefBuilderDataSnapshot<T, Base>;
export function makeTypedTestDataSnapshot<P extends string, Base = ZoomSenseFirebaseRoot, T = NestedValueOf<Base, P>>(
  testRunner: FeaturesList,
  path: P,
  fromData: T | null,
  toData: T | null
): Change<TypedRefBuilderDataSnapshot<T, Base>>;
export function makeTypedTestDataSnapshot<P extends string, Base = ZoomSenseFirebaseRoot, T = NestedValueOf<Base, P>>(
  testRunner: FeaturesList,
  path: P,
  fromData: T | null,
  toData?: T | null
): TypedRefBuilderDataSnapshot<T, Base> | Change<TypedRefBuilderDataSnapshot<T, Base>> {
  return toData
    ? Change.fromObjects(
        testRunner.database.makeDataSnapshot(fromData as any, path),
        testRunner.database.makeDataSnapshot(toData as any, path)
      )
    : testRunner.database.makeDataSnapshot(fromData as any, path);
}
