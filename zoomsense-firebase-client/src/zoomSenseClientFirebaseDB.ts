import * as firebase from "firebase";
import "vue";
import { PluginFunction } from "vue";
import { RTDBOptions } from "@posva/vuefire-core";
import {
  ChildOfDynamicStringKey,
  NestedValueOf,
  TypedFirebasePath,
  ZoomSenseFirebaseRoot
} from "@zoomsense/zoomsense-firebase";

export interface TypedDataSnapshot<T, Base = ZoomSenseFirebaseRoot, Exists extends boolean = boolean> extends firebase.database.DataSnapshot {
  child<P extends string>(path: P): TypedDataSnapshot<NestedValueOf<T, P>, Base>;
  forEach(action: (a: TypedDataSnapshot<T[keyof T], Base, true>) => boolean | void): boolean;
  ref: TypedReference<T, Base>;
  exists(this: TypedDataSnapshot<T,  Base, Exists>): this is TypedDataSnapshot<T,  Base, true>;
  val(): Exists extends true ? T : Exists extends false ? null : T | null;
}

export type TypedThenableReference<T, Base> = TypedReference<T, Base> & Promise<TypedReference<T, Base>>;

export type TypedQuery<T, Base = ZoomSenseFirebaseRoot> = Pick<TypedReference<T, Base>, keyof firebase.database.Query>;

export interface TypedReference<T, Base = ZoomSenseFirebaseRoot> extends firebase.database.Reference {
  ref: TypedReference<T, Base>;
  child<K extends string>(path: K): TypedReference<NestedValueOf<T, K>, Base>;
  root: TypedReference<Base, Base>;
  push(
    value?: ChildOfDynamicStringKey<T>,
    onComplete?: (a: Error | null) => any
  ): TypedThenableReference<ChildOfDynamicStringKey<T>, Base>;
  set(value: T, onComplete?: (a: Error | null) => any): Promise<unknown>;
  get(): Promise<TypedDataSnapshot<T, Base>>;
  update(values: T extends object ? Partial<T> : never, onComplete?: (a: Error | null) => any): Promise<unknown>;
  on(
    eventType: string,
    callback: (a: TypedDataSnapshot<T, Base>, b?: string | null) => unknown,
    cancelCallbackOrContext?: ((a: Error) => any) | Object | null,
    context?: Object | null
  ): (a: firebase.database.DataSnapshot | null, b?: string | null) => unknown;
  off(
    eventType?: string,
    callback?: (a: TypedDataSnapshot<T, Base>, b?: string | null) => any,
    context?: Object | null
  ): void;
  once(
    eventType: string,
    successCallback?: (a: TypedDataSnapshot<T, Base>, b?: string | null) => any,
    failureCallbackOrContext?: ((a: Error) => void) | Object | null,
    context?: Object | null
  ): Promise<TypedDataSnapshot<T, Base>>;
  endBefore(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  endAt(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  equalTo(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  isEqual(other: TypedQuery<T, Base> | null): boolean;
  limitToFirst(limit: number): TypedQuery<T, Base>;
  limitToLast(limit: number): TypedQuery<T, Base>;
  orderByChild(path: string): TypedQuery<T, Base>;
  orderByKey(): TypedQuery<T, Base>;
  orderByPriority(): TypedQuery<T, Base>;
  orderByValue(): TypedQuery<T, Base>;
  startAt(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
  startAfter(value: number | string | boolean | null, key?: string): TypedQuery<T, Base>;
}

/**
 * To get the full benefit of wrapping the Firebase db object with this class, you should also add the modified Vuefire
 * plugin by calling `Vue.use(typedVuefirePlugin)` and use `$typedRtdbBind` instead of `$rtdbBind`.
 */
export class ZoomSenseClientFirebaseDB<Base = ZoomSenseFirebaseRoot> implements firebase.database.Database {
  private readonly db: firebase.database.Database;
  app: firebase.app.App;

  constructor(db: firebase.database.Database) {
    this.db = db;
    this.app = db.app;
  }

  ref<T, P extends string>(path: TypedFirebasePath<T, P>): TypedReference<T, Base>;
  ref<P extends string>(path: P): TypedReference<NestedValueOf<Base, P>, Base>;
  ref(path: string) {
    return this.db.ref(path);
  }

  goOffline(): void {
    this.db.goOffline();
  }

  goOnline(): void {
    this.db.goOnline();
  }

  refFromURL(url: string): firebase.database.Reference {
    return this.db.refFromURL(url);
  }
}

/**
 * A custom Vue plugin which adds `$typedRtdbBind` to Vue.  This does exactly what Vuefire's `$rtdbBind` does, but with
 * more restrictive types: the Vue variable name must be of the same type as the (typed) Firebase ref.
 */
export const typedVuefirePlugin: PluginFunction<void> = (vue) => {
  if (!vue.prototype["$rtdbBind"]) {
    throw new Error(
      "You must call `Vue.use(rtdbPlugin)` (Vuefire's plugin) before calling `Vue.use(typedVuefirePlugin)`."
    );
  }
  vue.prototype["$typedRtdbBind"] = vue.prototype["$rtdbBind"];
};

declare module "vue/types/vue" {
  export interface Vue {
    $typedRtdbBind<T extends object, N extends keyof T>(
      this: T,
      name: N,
      reference: TypedReference<NonNullable<T[N]>>,
      options?: RTDBOptions
    ): Promise<TypedDataSnapshot<NonNullable<T[N]>>>;
  }
}
