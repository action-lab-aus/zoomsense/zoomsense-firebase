# Zoomsense Firebase types

This repository defines three NPM packages.
* `@zoomsense/zoomsense-firebase` defines Typescript types for the data stored in the Firebase RTDB for ZoomSense, and 
  some utility types.
* `@zoomsense/zoomsense-firebase-client` defines wrappers for Firebase types used in client code.
* `@zoomsense/zoomsense-firebase-server` defines wrappers for Firebase types used in server code.


## @zoomsense/zoomsense-firebase

### ZoomSense Types

The root type for ZoomSense's data stored in the Firebase RTDB is `ZoomSenseFirebaseRoot`. This interface is used by all
the following utility functions as the default starting point to compute the types of data stored in the RTDB 
referenced by string paths like `meetings/${userId}/${meetingId}`.

Changes or additions to the core data of ZoomSense should be merged into this project, but it doesn't make sense for the
custom data types of plugins to be merged here. Instead, plugins can use declaration merging to augment the interfaces
exported by this package in their local code.

For example, if your plugin stores its data under the Firebase path `data/plugins/myPlugin`, and you've defined a
Typescript type `MyPluginDataType` describing that data, you can find the interface corresponding to `data/plugins` in
this package (`ZoomSenseDataPlugins`) and create a `.d.ts` file alongside your Typescript sources which merges in your
own type:

```ts
import "@zoomsense/zoomsense-firebase"
import { MyPluginDataType } from "./myPluginTypes.ts"; // or whatever

declare module "@zoomsense/zoomsense-firebase" {
  export interface ZoomSenseDataPlugins {
    myPlugin: MyPluginDataType;
  }
}
```

The `myPlugin` key will be merged with the existing definition of `ZoomSenseDataPlugins`, and thereafter the below
utility functions will know how to infer the type of references to `data/plugins/myPlugin` and any path below that.

### Typed Firebase path strings

Sometimes you want to pass around a reference to a location in the database with a simple string, rather than a full-on
DB reference. To support such use cases, this package exports a type `TypedFirebasePath<Type, Path extends string>`
which is a string, but also has an associated `firebaseType` field of type `T` which tracks which ZoomSense type the
path accesses.

```ts
import { buildFirebasePath, appendFirebasePath } from "@zoomsense/zoomsense-firebase";
import { db } from "./myFileWotWrapsDbWithAppropriateClass";

const userId = 'whatever', meetingKey = 'whatever';
const meetingPath = buildFirebasePath(`meetings/${userId}/${meetingId}`);
// `meetingPath` is of type TypedFirebasePath<ZoomSenseMeeting, `meetings/${string}/${string}`>

console.log(meetingPath);
// Logs meetings/whatever/whatever

const meetingIdPath = appendFirebasePath(meetingPath, "id");
// `meetingIdPath` is of type TypedFirebasePath<number, `meetings/${string}/${string}/id`>

const zoomSensor1VersionPath = appendFirebasePath(meetingPath, `ZoomSensor_1/version`);
// `zoomSensor1VersionPath` is of type TypedFirebasePath<string, `meetings/${string}/${string}/ZoomSensor_1/version`>

const meetingRef = db.ref(meetingPath);
// meetingRef is of type TypedReference<ZoomSenseMeeting>
```

## Peer Dependencies

The base `@zoomsense/zoomsense-firebase` package is a peer dependency of the two other packages defined in this 
repository, `@zoomsense/zoomsense-firebase-client` and `@zoomsense/zoomsense-firebase-server`.  As a peer dependency, 
projects that import either the client or server package also needs to import `@zoomsense/zoomsense-firebase` 
explicitly.  This allows such projects to update to newer versions of the base `@zoomsense/zoomsense-firebase` 
project to get the latest versions of the ZoomSense types (which are anticipated to be the most common changes) 
without requiring the client and server packages to be re-published.

Both the client and server packages export wrappers for the corresponding Firebase DB object which override the `ref` 
method to return a `TypedReference<T>`, allowing them to know the type of the data referred to by the reference. 
There are two different packages because the client-side and server-side Firebase DB objects are of different types 
and have different dependencies.  Also, they export some distinct additional utility types and/or methods.

As much as possible, all methods and fields on the `TypedReference` are wrapped to track types, such as `child`, `push`,
`on` etc. The only field that still uses the default untyped Reference type is `TypedReference.parent`.

Note that Typescript can generally only infer the types of a `db.ref` if the string used to look up the value is a
literal. If you want to pass Firebase paths around as strings which are then passed to `db.ref`, use `TypedFirebasePath`
strings (see above).

### @zoomsense/zoomsense-firebase-server

The server-side package exports a wrapper class called `ZoomSenseServerFirebaseDB`.

```ts
import { ZoomSenseServerFirebaseDB } from "@zoomsense/zoomsense-firebase-server";
import * as admin from "firebase-admin";

const db = new ZoomSenseServerFirebaseDB(admin.database());

const userId = 'whatever', meetingKey = 'whatever';
const meetingRef = db.ref(`meetings/${userId}/${meetingKey}`);
// meetingRef is of type TypedReference<ZoomSenseMeeting>

const meeting = (await meetingRef.once("value")).val();
// meeting is of type ZoomSenseMeeting | null

meetingRef.child("ZoomSensor_1").once("value", (snapshot) => {
  // snapshot is of type ZoomSenseSensorMeetingRecord
})
```

In addition, you can wrap calls to `functions.database.ref` with the function `typedFirebaseFunctionRef` in order to
create triggers for DB changes which know the type of the DataSnapshot parameter.

```ts
import * as functions from "firebase-functions";
import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";

export const myOnCreateTrigger = typedFirebaseFunctionRef(functions.database.ref)(
  "meetings/{userId}/{meetingKey}"
)
  .onCreate(async (snapshot, context) => {
    // snapshot is of type TypedDataSnapshot<ZoomSenseMeeting>
  });
```

Another export is the `makeTypedTestDataSnapshot` function for unit testing, which uses `firebase-functions-test` to
build a data snapshot, but does type checking based on the path.

```ts
import * as initTest from "firebase-functions-test";
import * as admin from "firebase-admin";
import { makeTypedTestDataSnapshot } from "@zoomsense/zoomsense-firebase-server";

// Use emulator database
export const databaseURL = "http://localhost:9000/?ns=zoomsense-plugin-default-rtdb";

const projectConfig = {
  projectId: "zoomsense-plugin-example",
  databaseURL,
};

const testRunner = initTest(projectConfig);

admin.initializeApp({ databaseURL });

const userId = 'whatever', meetingKey = 'whatever';
const typedSnapshot = makeTypedTestDataSnapshot(
  testRunner,
  `meetings/${userId}/${meetingKey}`,
  {
    ...
    // This literal must match the shape of type ZoomSenseMeeting
  }
);

```

### @zoomsense/zoomsense-firebase/client

The client-side package exports a wrapper class called `ZoomSenseClientFirebaseDB`.

```ts
import * as firebase from "firebase/app";
import { ZoomSenseClientFirebaseDB } from "@zoomsense/zoomsense-firebase-client";

const firebaseConfig = { /* whatever */ };
firebase.initializeApp(firebaseConfig);
export const db = new ZoomSenseClientFirebaseDB(firebase.database());

// Usage as above
```

Since ZoomSense uses Vuefire to bind UI variables to database locations, this package also exports a custom plugin which
adds a `$typedRtdbBind` method to Vue instances.

```ts
import Vue from "vue";
import { rtdbPlugin } from "vuefire";
import { typedVuefirePlugin } from "@zoomsense/zoomsense-firebase-client";

Vue.use(rtdbPlugin);
Vue.use(typedVuefirePlugin); // This package's plugin must be registered after vuefire's rtdbPlugin
```

The `$typedRtdbBind` method does the same thing as `$rtdbBind` (and uses Vuefire's implementation under the covers), but
has more stringent type requirements, forcing the type of the bound UI variable to match the type of the Firebase
reference it is bound to.

```ts
import Vue from "vue";
import { db } from "../aboveExample";
import { ZoomSenseMeetings } from "@zoomsense/zoomsense-firebase";

export default Vue.extend({
  name: "TestComponent",
  data() {
    return {
      meetingsVariable: null as null | ZoomSenseMeetings,
      whitelist: null as null | string
    }
  },
  mounted() {
    this.$typedRtdbBind("meetingsVariable", db.ref("meetings")); // Ok
    this.$typedRtdbBind("undefinedVariable", db.ref("meetings")); // Error, undefinedVariable not a key of this Vue instance
    this.$typedRtdbBind("whitelist", db.ref("whitelist")); // Error, whitelist is of type string, should be string[]
  }
});
```
